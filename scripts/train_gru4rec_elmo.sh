#!/bin/bash
export PYTHONPATH="$HOME/repos/nn/nnrecsys"
source activate msemikin
python -m nnrecsys.amazon_emb --experiment gru4rec_elmo_bpr --image_embeddings False --batch_size 128 --eval_batch_size 128 --loss bpr --scores additive
