#!/bin/bash
export PYTHONPATH="$HOME/repos/nn/nnrecsys"
source activate msemikin
python -m nnrecsys.amazon_emb --experiment gru4rec_tfidf_bpr --image_embeddings False --batch_size 128 --eval_batch_size 128 --loss bpr --text_embeddings_type tfidf --scores additive
