#!/bin/bash
export PYTHONPATH="$HOME/repos/nn/nnrecsys"
source activate msemikin
python -m nnrecsys.data.amazon.scripts.review_text_elmo
python -m nnrecsys.data.amazon.scripts.gather_reviews
