#!/bin/bash
python -m nnrecsys.amazon_emb --experiment elmo_ce --image_embeddings False --batch_size 128 --eval_batch_size 128 --loss crossentropy --n_negatives -1 --reg_weight 0.001 --dropout 0.7 --lr 0.017
