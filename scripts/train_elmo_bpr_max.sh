#!/bin/bash
python -m nnrecsys.amazon_emb --experiment elmo_bpr_max --image_embeddings False --batch_size 32 --eval_batch_size 32 --loss bpr_max --n_negatives -1 --reg_weight 0.001 --dropout 0.7 --lr 0.01
