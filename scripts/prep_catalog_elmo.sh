#!/bin/bash
export PYTHONPATH="$HOME/repos/nn/nnrecsys"
source activate msemikin
python -m nnrecsys.data.amazon.scripts.catalog_text_elmo
python -m nnrecsys.data.amazon.scripts.gather_catalog
