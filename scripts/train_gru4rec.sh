#!/bin/bash
export PYTHONPATH="$HOME/repos/nn/nnrecsys"
source activate msemikin
python -m nnrecsys.amazon_emb --experiment gru4rec --identity_embeddings True --image_embeddings False --text_embeddings False --lr 0.01 --batch_size 128 --scores additive --loss bpr
